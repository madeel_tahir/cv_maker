import { useState } from 'react';
import TemplateSelection from './components/TemplateSelection/TemplateSelection';

function App() {
  const [selectedTemplate,setSelectedTemplate]=useState("");


  return (
    <div className="container">
      <TemplateSelection selectedTemplate={selectedTemplate} setSelectedTemplate={setSelectedTemplate} />

      {/* <div onClick={()=>{
        alert(selectedTemplate);
      }}>Test</div> */}
    </div>
  );
}

export default App;
