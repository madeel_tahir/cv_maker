import 'bootstrap/dist/css/bootstrap.min.css';
import Carousel from 'react-bootstrap/Carousel';
import {Image, Button} from 'react-bootstrap';
import './TemplateSelection.css';


function TemplateSelection({selectedTemplate,setSelectedTemplate}){

    return (
        <>
        <div className="row mt-4 mb-4">
            <div className="col-12">
                <div className="row">
                <div className="col-12">
                    <div className="d-flex justify-content-center">
                    <h1 className="text-center" style={{marginRight:"30px"}}>Choose Template to continue</h1>
                    {(selectedTemplate) && <Button className="btn btn-success">Next</Button>}
                    </div>
                    
                </div>
                
                </div>
                
            </div>
        </div>
        
        <Carousel style={{backgroundColor:"black",padding:"20px"}}>
  <Carousel.Item>
    <Image
      className={"rounded d-block mx-auto img-fluid"+(selectedTemplate===1? " selectedTemplate":"")}
      src={process.env.PUBLIC_URL+"/images/template-1.jpg"}
      alt="Template 1"
      onClick={()=>{
          setSelectedTemplate(1);
        }
      }
      style={{cursor:'pointer'}}
    />
  </Carousel.Item>
  <Carousel.Item>
    <Image
      className={"rounded d-block mx-auto img-fluid"+(selectedTemplate===2? " selectedTemplate":"")}
      src={process.env.PUBLIC_URL+"/images/template-2.jpg"}
      alt="Template 2"
      onClick={()=>{
        setSelectedTemplate(2);
      }
    }
    style={{cursor:'pointer'}}
    />
  </Carousel.Item>
  <Carousel.Item>
    <Image
      className={"rounded d-block mx-auto img-fluid"+(selectedTemplate===3? " selectedTemplate":"")}
      src={process.env.PUBLIC_URL+"/images/template-3.jpg"}
      alt="Template 3"
      onClick={()=>{
        setSelectedTemplate(3);
      }
    }
    style={{cursor:'pointer'}}
    />
  </Carousel.Item>
</Carousel>
        </>
    );
}

export default TemplateSelection;